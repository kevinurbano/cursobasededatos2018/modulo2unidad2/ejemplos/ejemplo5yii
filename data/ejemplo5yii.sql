﻿DROP DATABASE IF EXISTS ejemplo5yii;
CREATE DATABASE IF NOT EXISTS ejemplo5yii;
USE ejemplo5yii;

CREATE OR REPLACE TABLE peliculas(
  id int AUTO_INCREMENT,
  titulo varchar(50),
  descripcion text,
  foto varchar(50),
  PRIMARY KEY(id)
);
